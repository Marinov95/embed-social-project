import cardData from "../data/data.json" assert { type: "json" };

// NOTE: Has to be run on a Live server. Otherwise will get a CORS Error(expected behavior). Alternatives are using the fetch API if we host the JSON or just making the JSON file a JS File and exporting the array, but that defeats the purpose of it being JSON.

let currentCards = 4;

const addFourCards = () => {
  const container = document.querySelector(".card-container");
  for (let i = currentCards - 4; i < currentCards && i < cardData.length; i++) {
    const card = createCard(cardData[i]);
    container.append(card);
  }
};

const createCard = (data) => {
  const card = document.createElement("div");
  card.classList.add("card");

  const cardInner = document.createElement("div");
  cardInner.classList.add("card-inner");

  cardInner.append(createCardHeader(data));
  cardInner.append(createCardBody(data));
  cardInner.append(createCardFooter(data));

  card.append(cardInner);

  // Opening the Modal on Click except if it is the like button.
  card.addEventListener("click", (e) => {
    if (
      e.target.classList.contains("like-div") ||
      e.target.classList.contains("like-counter") ||
      e.target.offsetParent.classList.contains('social-media')
    ) {
      return;
    }
    openAndFillModal(card);
  });
  return card;
};

const createCardHeader = (data) => {
  const cardHeader = document.createElement("header");
  cardHeader.classList.add("card-header");

  const profileImg = document.createElement("img");
  profileImg.classList.add("profile-img");
  profileImg.src = data.profile_image;

  cardHeader.append(profileImg);

  const infoDiv = document.createElement("div");
  infoDiv.classList.add("info");

  const author = document.createElement("h2");
  author.classList.add("author");
  author.innerText = data.name;

  const dateEl = document.createElement("p");
  dateEl.classList.add("date");

  const date = new Date(data.date);
  const year = date.getFullYear(date);
  const month = date.getMonth(date);
  const day = date.getDate(data);

  const monthShorthands = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];

  dateEl.innerText = `${day} ${monthShorthands[month]} ${year}`;

  infoDiv.append(author, dateEl);
  cardHeader.append(infoDiv);

  const socialMedia = document.createElement("div");
  socialMedia.classList.add("social-media");

  const socialMediaLink = document.createElement("a");
  socialMediaLink.href = data.source_link;
  socialMediaLink.setAttribute("target", "_blank");

  const socialMediaImg = document.createElement("img");
  if (data.source_type === "facebook") {
    socialMediaImg.src = "./assets/img/facebook.svg";
  } else {
    socialMediaImg.src = "./assets/img/instagram-logo.svg";
  }

  socialMediaLink.append(socialMediaImg);
  socialMedia.append(socialMediaLink);
  cardHeader.append(socialMedia);

  return cardHeader;
};

const createCardBody = (data) => {
  const cardBody = document.createElement("div");
  cardBody.classList.add("card-body");

  const cardImg = document.createElement("img");
  cardImg.src = data.image;

  const caption = document.createElement("p");

  // Adding placeholder text if the caption is empty because otherwise it looks out of place
  if (data.caption) {
    caption.innerText = data.caption;
  } else {
    caption.innerText =
      "This user has not added a caption, click on the icon on the top right to go straight to the post.";
    caption.style.fontStyle = "italic";
  }
  cardBody.append(cardImg);
  cardBody.append(caption);

  return cardBody;
};

const createCardFooter = (data) => {
  const cardFooter = document.createElement("footer");
  cardFooter.classList.add("card-footer");

  const wrapper = document.createElement("div");

  const likeButton = document.createElement("object");
  likeButton.classList.add("like-button");
  likeButton.data = "./assets/img/heart.svg";

  const likeCounter = document.createElement("span");
  likeCounter.classList.add("like-counter");
  likeCounter.innerText = data.likes;
  wrapper.append(likeButton, likeCounter);
  wrapper.setAttribute("class", "like-div");
  // Wrapping the button and the like button in a div to make it possible to add an event listener, cannot directly be added to the object element

  const onLikeClick = (e) => {
    const svg = likeButton.contentDocument.querySelector("svg");
    if (wrapper.classList.contains("clicked")) {
      likeCounter.innerText = parseInt(likeCounter.innerText) - 1;
      wrapper.classList.remove("clicked");
      svg.setAttribute("fill", "none");
      return;
    }
    likeCounter.innerText = parseInt(likeCounter.innerText) + 1;
    wrapper.classList.add("clicked");
    svg.setAttribute("fill", "red");
  };

  wrapper.addEventListener("click", onLikeClick);

  cardFooter.append(wrapper);

  return cardFooter;
};

const modal = document.querySelector(".modal");
const modalLeft = document.querySelector(".modal-left");
const modalRight = document.querySelector(".modal-right");

const openAndFillModal = (card) => {
  modal.classList.add("modal-opened");
  // cloning all the elements i want into the modal
  modalLeft.append(card.querySelector(".card-body img").cloneNode(true));
  modalRight.append(card.querySelector(".card-header").cloneNode(true));
  modalRight.append(card.querySelector(".card-body p").cloneNode(true));
  modalRight.append(
    card.querySelector(".card-footer .like-div").cloneNode(true)
  );
};

const closeAndClearModal = () => {
  // Adding a closing animation and reseting the modal content after it finishes
  modal.classList.add("closing");
  modal.addEventListener(
    "animationend",
    (e) => {
      modal.classList.remove("closing", "modal-opened");
      modalRight.innerHTML = "";
      modalLeft.innerHTML = "";
    },
    { once: true }
  );
};

window.addEventListener("click", (e) => {
  if (e.target === modal) {
    closeAndClearModal();
  }
});

const loadMoreButton = document.querySelector("#load-more");

loadMoreButton.addEventListener("click", (e) => {
  currentCards += 4;
  if (currentCards >= cardData.length) {
    loadMoreButton.classList.add("button-disabled");
  }
  addFourCards();
});

addFourCards();
